#if !defined VECTOR3_H_INCLUDED
#define VECTOR3_H_INCLUDED

#include <complex>
#include <assert.h>
#include "point.h"

// Немного typedef'ов
template<typename T> class Vector3;
typedef Vector3<double> DVector3;


// Класс трехмерный вектор
template<typename T>
class Vector3 {
public:
  // Элементы
  T x, y, z;
  // Констуктор по умолчанию
  Vector3 ();
  // Конструктор из трех элементов
  template<typename U, typename V, typename R>
  Vector3 (U x, V y, R z);
  // Конструктор из точки
  template<typename U>
  Vector3 (const Point<U> & p);
  // Конструктор из двух точек - начала и конца
  template<typename U>
  Vector3 (const Point<U> & beg_, const Point<U> & end_);
  // Конструктор из другого вектора
  template<typename U>
  Vector3 (const Vector3<U> & v);
  // Кастование к точке
  Point<T> pnt () const;
  // Норма вектора
  double norm () const;
  // Квадрат нормы вектора
  double norm2 () const;
  // Векторное произведение
  Vector3<T> cross (const Vector3<T> & other) const;
  // Операторы типа "скобка"
  T & operator [] (size_t i);
  T operator [] (size_t i) const;
  // Скалярное произведение
  T operator * (const Vector3<T> & other) const;
  template<typename U, typename V>
  V operator * (const Vector3<U> & other) const;
  // Сложение и вычитание векторов
  Vector3<T> operator + (const Vector3<T> & other) const;
  Vector3<T> operator - (const Vector3<T> & other) const;
  // Деление вектора на число
  Vector3<T> operator / (const T & a) const;
  // Умножение числа на вектор
  template<typename U>
  friend Vector3<U> operator * (const U & a, const Vector3<U> & vec);
  template<typename U, typename V, typename R>
  friend Vector3<R> operator * (const U & a, const Vector3<V> & vec);
  // Умножение матрицы на вектор
//    template<typename U, typename V>
//    friend Vector3<U> operator * (const matrix_t<V, 3, 3> & matr, const Vector3<U> & vec);
    // Вывод
  template<typename U>
  friend std::ostream & operator << (std::ostream & os, const Vector3<U> & a);
  // Получение сопряженного вектора
  Vector3<T> cj () const;
};

// Констуктор по умолчанию
template<typename T>
Vector3<T>::Vector3 () {
  x = y = z = static_cast<T>(0);
}

// Конструктор из трех элементов
template<typename T>
template<typename U, typename V, typename R>
Vector3<T>::Vector3 (U x, V y, R z) {
  this->x = static_cast<T>(x);
  this->y = static_cast<T>(y);
  this->z = static_cast<T>(z);
}

// Конструктор из точки
template<typename T>
template<typename U>
Vector3<T>::Vector3 (const Point<U> & p) {
  x = static_cast<T>(p.x);
  y = static_cast<T>(p.y);
  z = static_cast<T>(p.z);
}

// Конструктор из двух точек - начала и конца
template<typename T>
template<typename U>
Vector3<T>::Vector3 (const Point<U> & beg_, const Point<U> & end_) {
  x = static_cast<T>(end_.x - beg_.x);
  y = static_cast<T>(end_.y - beg_.y);
  z = static_cast<T>(end_.z - beg_.z);
}

// Конструктор из другого вектора
template<typename T>
template<typename U>
Vector3<T>::Vector3 (const Vector3<U> & v) {
  x = static_cast<T>(v.x);
  y = static_cast<T>(v.y);
  z = static_cast<T>(v.z);
}

// Кастование к точке
template<typename T>
Point<T> Vector3<T>::pnt () const {
  return Point<T> (x, y, z);
}

// Векторное произведение
template<typename T>
Vector3<T> Vector3<T>::cross (const Vector3<T> & other) const {
  return Vector3<T> (y * other.z - z * other.y,
                    z * other.x - x * other.z,
                    x * other.y - y * other.x);
}

// Операторы типа "скобка"
template<typename T>
T & Vector3<T>::operator [] (size_t i) {
  assert (i < 3);
  switch (i) {
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
  }
  return x;
}

template<typename T>
T Vector3<T>::operator [] (size_t i) const {
  assert (i < 3);
  switch (i) {
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
  }
  return 0;
}

// Скалярное произведение
template<typename T>
T Vector3<T>::operator * (const Vector3<T> & other) const {
  return x * other.x + y * other.y + z * other.z;
}

// Сложение и вычитание векторов
template<typename T>
Vector3<T> Vector3<T>::operator + (const Vector3<T> & other) const {
  return Vector3<T> (x + other.x, y + other.y, z + other.z);
}

template<typename T>
Vector3<T> Vector3<T>::operator - (const Vector3<T> & other) const {
  return Vector3<T> (x - other.x, y - other.y, z - other.z);
}

// Деление вектора на число
template<typename T>
Vector3<T> Vector3<T>::operator / (const T & a) const {
  return Vector3<T> (x / a, y / a, z / a);
}

// Умножение числа на вектор
template<typename U>
Vector3<U> operator * (const U & a, const Vector3<U> & vec) {
  return Vector3<U> (a * vec.x, a * vec.y, a * vec.z);
}

// Вывод
template<typename U>
std::ostream & operator << (std::ostream & os, const Vector3<U> & a) {
  os << "{ " << a.x << ", " << a.y << ", " << a.z << " }";
  return os;
}

// Норма вектора (действительная)
template<>
double Vector3<double>::norm () const;

// Квадрат нормы вектора (действительной)
template<>
double Vector3<double>::norm2 () const;

// Норма вектора (комплексная)
template<>
double Vector3< std::complex<double> >::norm () const;

// Квадрат нормы вектора (комплексной)
template<>
double Vector3< std::complex<double> >::norm2 () const;

// Скалярное произведение
template<> template<>
std::complex<double> Vector3<double>::operator * (const Vector3< std::complex<double> > & other) const;

template<> template<>
std::complex<double> Vector3< std::complex<double> >::operator * (const Vector3<double> & other) const;

// Умножение числа на вектор
Vector3< std::complex<double> > operator * (const double & a, const Vector3< std::complex<double> > & vec);

Vector3< std::complex<double> > operator * (const std::complex<double> & a, const Vector3<double> & vec);

// Получение сопряженного вектора
template<>
Vector3< std::complex<double> > Vector3< std::complex<double> >::cj () const;

#endif // VECTOR3_H_INCLUDED
