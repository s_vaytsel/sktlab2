#ifndef INVERSE_H
#define INVERSE_H

#include <vector>
#include <array>
#include <string>
#include <utility>

#include <fstream>
#include "vector3.h"
#include "direct.h"

class InverseConfig {
public:
  bool use_alpha;     // Использовать регуляризацию по альфа
  bool use_gamma;     // Использовать регуляризацию по гамма
  double alpha0;      // Начальное значение alpha
  double dalpha;      // Шаг увеличения alpha
  double alpha_coeff; // Во сколько раз может возрати значение функционала
  double gamma0;      // Начальное значение gamma
  double dgamma;      // Шаг увеличения gamma
  double gamma_coeff; // На сколько порядков может возрасти значение функционала
  double gamma_diff;  // Разница между соседями, которую считаем уже разницей
  InverseConfig ();
  friend std::istream & operator >> (std::istream & is, InverseConfig & a);
  friend std::ostream & operator << (std::ostream & os, const InverseConfig & a);
};

class Inverse {
public:
  void input (const std::string & fn_area, const std::string & fn_receivers, const std::string & fn_config);
  void calc ();

protected:
  size_t A_n, b_n, L_n, L_m;
  double** A;
  double* b;
  DVector3** L;
  size_t K, N;
  std::vector<std::pair<DPoint, DVector3> > receivers;

  void make_L ();
  void make_A ();
  void make_B ();
  Area ar;

  double alpha;
  std::vector<DVector3> gamma;

  void solve_gauss (double** matrix, size_t matrix_n, double* rpart, double* solution) const;
  double calc_functional_FI (const double* solution);
  double calc_functional_FI_alpha_gamma (const double* solution);

  void print_solution (const double* solution, const std::string & filename);
  InverseConfig cfg;
};

#endif // INVERSE_H
