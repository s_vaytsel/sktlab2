#ifndef DIRECT_H
#define DIRECT_H

#include "point.h"
#include "vector3.h"

#if !defined M_PI
#define M_PI 3.14159265358979323846
#endif

/*   _____________
 *  /|6          /|7    ^ z
 * /____________/ |     |  ^ y
 * |4|          |5|     | /
 * | |          | |     |/----> x
 * | |          | |
 * | |          | |
 * | |__________|_|
 * | /2         | /3
 * |/___________|/
 * 0            1
 */

/** Список соседей:
  * 0 - слева
  * 1 - справа
  * 2 - спереди
  * 3 - сзади
  * 4 - снизу
  * 5 - сверху
  */

class Cube {
public:
  DPoint * nodes[8];
  DVector3 p;
  double mes;
  DPoint barycenter;
  Cube * neighbor[6];
  Cube ();
  void init ();
  DVector3 get_B (const DPoint & x) const;

  double gauss_weights[27];
  DPoint gauss_points[27];
  double jacobian;

  size_t num;
};

class Include {
public:
  double x_min, x_max;
  double y_min, y_max;
  double z_min, z_max;
  DVector3 p;
  bool inside (const Cube & p) const;
  friend std::istream & operator >> (std::istream & is, Include & a);
};

class Area {
public:
  size_t nodes_size;
  size_t cubes_size;

  DPoint* nodes;
  Cube* cubes;

  DVector3 get_B (const DPoint & x) const;
  double get_abs_B (const DPoint & x) const;
  void generate (const std::string & filename);

  ~Area();
};

#endif // DIRECT_H
