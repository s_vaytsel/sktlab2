#include "inverse.h"
#include <iostream>

using namespace std;

int main (int argc, char** argv) {
  if (argc != 4) {
    cout << "./app.exe area.txt receivers.txt config.txt" << endl;
    return 1;
  }

  Inverse solver;
  solver.input (argv[1], argv[2], argv[3]);
  solver.calc ();

  return 0;
}
