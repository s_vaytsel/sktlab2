#if !defined POINT_H_INCLUDED
#define POINT_H_INCLUDED

#include <complex>
#include <assert.h>

template<typename T> class Point;

typedef Point<double> DPoint;
typedef Point< std::complex<double> > CPoint;

// Класс точка
template<typename T> class Point {
public:
  // Координаты
  T x, y, z;
  // Номер точки
  size_t num;
  // Конструктор по умолчанию
  Point ();
  // Конструктор по трем координатам
  template<typename U>
  Point (U x, U y, U z);
  // Конструктор по трем координатам и номеру
  template<typename U>
  Point (U x, U y, U z, size_t num);
  // Конструктор из другой точки
  template<typename U>
  Point (const Point<U> & p);
  // Операторы типа "скобка"
  T & operator [] (size_t i);
  T operator [] (size_t i) const;
  // Оператор меньше (по номеру)
  template<typename U>
  bool operator < (const Point<U> & t) const;
  // Оператор равенства (по номеру)
  template<typename U>
  bool operator == (const Point<U> & t) const;
  // Оператор присваивания
  Point<T> & operator = (const Point<T> & other);
  template<typename U>
  Point<T> & operator = (const Point<U> & other);
  // Вывод
  template<typename U>
  friend std::ostream & operator << (std::ostream & os, const Point<U> & a);
  // Проверка, лежит ли точка внутри параллелепипеда (для дерева)
  bool inside (double x0, double x1, double y0, double y1, double z0, double z1) const;
};

// Конструктор по умолчанию
template<typename T>
Point<T>::Point () {
  x = y = z = static_cast<T>(0);
  num = 0;
}

// Конструктор по трем координатам
template<typename T>
template<typename U>
Point<T>::Point (U x, U y, U z) {
  this->x = static_cast<T>(x);
  this->y = static_cast<T>(y);
  this->z = static_cast<T>(z);
  num = 0;
}

// Конструктор по трем координатам и номеру
template<typename T>
template<typename U>
Point<T>::Point (U x, U y, U z, size_t num) {
  this->x = static_cast<T>(x);
  this->y = static_cast<T>(y);
  this->z = static_cast<T>(z);
  this->num = num;
}

// Конструктор из другой точки
template<typename T>
template<typename U>
Point<T>::Point (const Point<U> & p) {
  x = static_cast<T>(p.x);
  y = static_cast<T>(p.y);
  z = static_cast<T>(p.z);
  num = p.num;
}

// Операторы типа "скобка"
template<typename T>
T & Point<T>::operator [] (size_t i) {
  assert (i < 3);
  switch (i) {
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
  }
  return x;
}

template<typename T>
T Point<T>::operator [] (size_t i) const {
  assert (i < 3);
  switch (i) {
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
  }
  return 0;
}

// Оператор меньше (по номеру)
template<typename T>
template<typename U>
bool Point<T>::operator < (const Point<U> & t) const {
  return num < t.num;
}

// Оператор равенства (по номеру)
template<typename T>
template<typename U>
bool Point<T>::operator == (const Point<U> & t) const {
  return num == t.num;
}

// Оператор присваивания
template<typename T>
Point<T> & Point<T>::operator = (const Point<T> & other) {
  if (this != &other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->num = other.num;
  }
  return *this;
}

template<typename T>
template<typename U>
Point<T> & Point<T>::operator = (const Point<U> & other) {
  this->x = static_cast<T>(other.x);
  this->y = static_cast<T>(other.y);
  this->z = static_cast<T>(other.z);
  this->num = other.num;
  return *this;
}

// Вывод
template<typename U>
std::ostream & operator << (std::ostream & os, const Point<U> & a) {
  os << "{ " << a.x << ", " << a.y << ", " << a.z << " }";
  return os;
}

// Проверка, лежит ли точка внутри параллелепипеда (для дерева)
template<>
bool Point<double>::inside (double x0, double x1, double y0, double y1, double z0, double z1) const;

#endif // POINT_H_INCLUDED
